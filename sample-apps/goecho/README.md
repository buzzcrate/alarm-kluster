# goecho

Sample application designed to be built on an ARM node and eventually end up
on the cluster. All steps are assumed to be done on an ARM platform. 

## Prerequisites 

Assumed to be done on an ALARM based platform either physical or virtual. 
Package dependencies are buildah, base-devel, and go. Please be careful
as the image building may take a considerable amount of storage, base 
ALARM images with a pacstrap of base are around 1 gig alone. 

Command to be run prior: `pacman -S base-devel buildah go`

## Build Go code

Change directory into the gocode folder. Run `go build webServer.go`. Copy 
the binary `webServer` to the buildah folder. 

## Buildah bear... image

In your best Bostonian accent, cd to the buildah folder and copy over the 
webServer if it hasn't been copied over already. One should edit the file 
`buildah-me.sh` file (author!), then run the file. This script might take 
a minute or two depending on what ARM system you're running. 

Once done, push the image to a docker registry some place off platform. 
At the time of this documentation, having k3s work with an insecure 
registry was futile. Docker pub has free public offerings so go have 
a whicked good time. 

## Deploy time

This step can be done either on or off platform. All that is needed is a 
favorite kubectl client or substitute. Simply do the following in order. 

```bash
kubectl apply -f goecho-deploy.yaml
kubeclt apply -f goecho-svc.yaml
```

## Test it! 

TODO: Log a successful test

