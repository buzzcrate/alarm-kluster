#!/usr/bin/bash
: `
Presently builds the smallest images from pacstrap with minimal effort, then 
places the pre-built go program 'webServer' into the container. Container cmd
and entrypoint seems to not be working as expected as of this time as desired.
`

gosmall=$(buildah from scratch)
mntsmall=$(buildah mount $gosmall)
pacstrap -i -c $mntsmall bash busybox
buildah run $gosmall -- busybox --install -s
buildah copy $gosmall webServer /.
buildah unmount $mntsmall

# Set configurations for the container
buildah config --cmd webServer $gosmall # defaults to port 8001
buildah config --author DagoRed
buildah config --label Name=gosmallecho $gosmall 
buildah config --entrypoint '["webServer","8080"]' $gosmall
buildah commit $gosmall alarm:gosmall
