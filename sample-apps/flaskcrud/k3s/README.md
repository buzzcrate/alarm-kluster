# Deployments

In this directory are listed are the generic deployments. The exact tweaks from 
the demostrations of this project are noted in the specific folder. 

# Installation
1. `kubectl apply -f postgres-deployment.yml`
1. `kubectl apply -f flask-deployment.yml`
1. `kubectl apply -f flask-service.yml`
1. `kubectl apply -f flask-deployment.yml`
