"""Init for bookmanager"""

from bookmanager import db
db.create_all()
# If the database is the point of failure at first test, 
# use the following line in sqlite's database:
# CREATE TABLE book(title String[80] PRIMARY KEY);

exit()