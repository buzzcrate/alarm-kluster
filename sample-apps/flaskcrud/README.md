# Flask Crud

Sample application using Python Flask to make a basic CRUD app. `bookmanager.py`
is used getting an idea how the app should run locally with flask and using 
SqlLite as a database. `bookcontainer.py` shows how that app is modified 
slightly to make it a container native app with a postgresql database. 

## Prerequisites

Assumed to be built on ALARM if one wishes to make the container images 
EXACTLY as presented or demo'd to any audience. Otherwise, a cluster 
built on ARM based devices should be able to deploy the app in the k3s
folder without touching any of the python or templates (as the 
containers are uploaded to dockerhub). 

It is STRONGLY recommended to use mettallb, se the ClusterAdditions in 
this repo. 

## Testing Application

For local testing, all that one needs is SqlLite installed locally and 
python running locally with the packages installed from the 
`requirements.txt` file. When testing in a container for `bookcontainer.py`
the `flask-deploy.yml` file has some lines commented out to launch the 
container in a sleep state to test the python app. 

## Buildah bear... image 

Run the alarm-bookdemo.sh script on an Arch Linux ARM device using buildah 
(just like with go-echo). 

## Deploy time

To deploy, pic either the generic or specific deployment from the k3s folder
and install in this order. 

1. `kubectl apply -f postgres-deployment.yml`
1. `kubectl apply -f flask-deployment.yml`
1. `kubectl apply -f flask-service.yml`
1. Optionally: `kubectl apply -f flask-ingress.yml` pending how your service file is set up.