#!/usr/bin/bash
: '
Simplified and a little bloated of a container with bash,
python, and busybox. 
'
imagename='alarm-bookdemo'

newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount $newcontainer)

# Edit next line as desired for adjusting package in the image
pacstrap -i -c $scratchmount python busybox python-setuptools python-pip ca-certificates python-psycopg2
mkdir -p $scratchmount/bookdemo
cp -r bookcontainer.py __init__.py templates/ requirements.txt $scratchmount/bookdemo/
buildah run $newcontainer -- pip install -r /bookdemo/requirements.txt
buildah unmount $newcontainer

# Example of configurations to apply to the container
buildah config --author "Will Christensen" $newcontainer
buildah config --created-by "dagoredcode" $newcontainer
buildah config --label name=$imagename $newcontainer

# Save Container as a future container image
buildah commit $newcontainer $imagename

# Stop running image
buildah rm $newcontainer

