#!/usr/bin/bash
: `
This file will essentially take a raw rootfs and build a container. This 
container is NOT IDEAL for deploying to k3s, however it serves as a great
base for building future containers. If one wishes to build something 
from scratch with parity to the root file systems, this is where
one should start. 
`
imagename='alarm-base'

newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount $newcontainer)

# Edit next line as desired for adjusting package in the image
pacstrap -i -c $scratchmnt base
buildah unmount $newcontainer

# Example of configurations to apply to the container
buildah config --author "ALARM-acist" $newcontainer
buildah config --created-by "GitLabber" $newcontainer
buildah config --label name=$imagename $newcontainer

# Save Container as a future container image
buildah commit $newcontainer $imagename

# Stop running image
buildah rm $newcontainer
