# k3s-git

After playing around with k3s from the aur, I made a few changes to make my 
builds easier for me. Deployment of this package will be in conjunction 
with an ansible role specifically for deploying fresh builds.


## Dependencies 

1. Must be built on an ARM board or properly emulated ARM hardware.
1. Add the docker group to the user. `usermod -a -G docker <username>`
1. Install docker. `pacman -S docker`
1. Start the docker service `systemctl start docker`
1. Run `makepkg` again. 

*NOTE*: If docker fails due to not getting a socket, log out and log back in again then attempt build. 

