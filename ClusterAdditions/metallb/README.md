# Using MetalLB

Instead of relying on the simplistic `servicelb` that is a part of K3S, you
can make use of the [MetalLB][metallb] project, if you have control of the
network space the cluster is in.

See [the project's page][metallb] for further details.

## Installing MetalLB

Effectively, follow the [official install instructions][install].

Short hand, this repository includes `0.7.3` as [metallb.yaml](metallb.yaml),
as well as a [sample configuration](config.yaml) for [Layer 2][layer2] usage.

```
kubectl apply -f metallb.yaml
kubectl apply -f config.yaml
```

## Configure K3S

To make sure that `Service`s of `type: LoadBalancer` make use of MetalLB,
you should ensure that your `k3s.service` on the master node includes
`--no-deploy servicelb` to disable that feature. This _is not required_
in `k3s-agent.service` on further nodes.

Be sure to reload the service, and restart `k3s.service`.

```
systemctl daemon-reload
systemctl restart k3s.service
```

## Configuration

The sample configuration is configured with 2 sections of the `192.168.8.x/24`
subnet. Edit [config.yaml](config.yaml) to make any changes you wish.

See the [MetalLB Layer 2 documentation][layer2] for extensive details on
[address pool configuration][pool].

[install]: https://metallb.universe.tf/installation/
[layer2]: https://metallb.universe.tf/configuration/#layer-2-configuration
[pool]: https://metallb.universe.tf/configuration/#advanced-address-pool-configuration
[metallb]: https://metallb.universe.tf/

